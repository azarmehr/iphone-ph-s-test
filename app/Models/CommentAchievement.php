<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentAchievement extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'order',
        'min_required_comments',
        'next_achievement_id',
    ];

    public function getNextAchievementAttribute()
    {
        return CommentAchievement::where('order', '>', $this->order)->orderBy('order','asc')->first();
    }
}
