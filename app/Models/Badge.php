<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'order',
        'min_required_achievements'
    ];

    public function getNextBadgeAttribute()
    {
        return Badge::where('order', '>', $this->order)->orderBy('order','asc')->first();
    }
}
