<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LessonAchievement extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'order',
        'min_required_lesson',
        'next_achievement_id',
    ];

    public function getNextAchievementAttribute()
    {
        return LessonAchievement::where('order', '>', $this->order)->orderBy('order','asc')->first();
    }
}
