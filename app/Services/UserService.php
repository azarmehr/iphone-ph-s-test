<?php

namespace App\Services;

use App\Models\Badge;
use App\Models\Lesson;
use App\Models\User;

class UserService
{
    public function __construct(protected AchievementService $achievementService)
    {
    }

    public function processUserLessonWatched(User $user, Lesson $lesson)
    {
        if ($user->lessons()->where('id', $lesson->id)->doesntExist()){
            $user->lessons()->attach($lesson, ['watched' => true]);
        }

        $this->achievementService->checkForUserLessonAchievements($user);
    }

    public function getUserAchievementsList(User $user)
    {
        $lessonAchievements = $user->lessonAchievements->pluck('title')->toArray();
        $commentAchievements = $user->commentAchievements->pluck('title')->toArray();

        return array_merge($lessonAchievements, $commentAchievements);
    }

    public function getUserNextAvailableAchievements(User $user)
    {
        $currentLessonAchievement = $this->getUserCurrentLessonAchievement($user);
        $currentCommentAchievement = $this->getCurrentCommentAchievement($user);

        return [
            $currentCommentAchievement->next_achievement->title,
            $currentLessonAchievement->next_achievement->title,
        ];
    }

    public function getUserCurrentLessonAchievement(User $user)
    {
        return $user->lessonAchievements()
            ->orderBy('order','desc')
            ->first();
    }

    public function getCurrentCommentAchievement(User $user)
    {
        return $user->commentAchievements()
            ->orderBy('order','desc')
            ->first();
    }

    public function getCurrentBadge(User $user)
    {
         $currentBadge = $user->badges()->orderBy('order','desc')->first();

         if (!$currentBadge){
             $currentBadge = Badge::where('slug', 'beginner')->first();
             $user->badges()->attach($currentBadge,[
                 'created_at' => now(),
                 'updated_at' => now()
             ]);
         }

         return $currentBadge;
    }
}
