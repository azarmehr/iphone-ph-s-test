<?php

namespace App\Services;

use App\Events\AchievementUnlocked;
use App\Models\Comment;
use App\Models\CommentAchievement;
use App\Models\LessonAchievement;
use App\Models\User;

class AchievementService
{
    public function checkForUserLessonAchievements(User $user)
    {
        $achievableAchievement = LessonAchievement::where('min_required_lesson', '<=', $user->watched()->count())
            ->orderBy('min_required_lesson','desc')
            ->first();

        $userHasAchievedTheAchievement = $user->lessonAchievements()
            ->wherePivot('lesson_achievement_id', $achievableAchievement->id)
            ->exists();

        if (! $userHasAchievedTheAchievement){
            $user->lessonAchievements()->attach($achievableAchievement, [
                'created_at' => now(),
                'updated_at' => now()
            ]);

            AchievementUnlocked::dispatch($achievableAchievement->title, $user);
        }
    }

    public function checkForUserCommentAchievement(Comment $comment)
    {
        $user = $comment->user;
        $achievableAchievement = CommentAchievement::where('min_required_comments', '<=', $user->comments()->count())
            ->orderBy('min_required_comments','desc')
            ->first();

        $userHasAchievedTheAchievement = $user->commentAchievements()
            ->wherePivot('comment_achievement_id', $achievableAchievement->id)
            ->exists();

        if (! $userHasAchievedTheAchievement){
            $user->commentAchievements()->attach($achievableAchievement, [
                'created_at' => now(),
                'updated_at' => now()
            ]);

            AchievementUnlocked::dispatch($achievableAchievement->title, $user);
        }
    }

    public function calculateTotalUserAchievement(User $user)
    {
        $commentAchievement = $user->commentAchievements()->count();
        $lessonAchievement = $user->lessonAchievements()->count();

        return $commentAchievement + $lessonAchievement;
    }
}
