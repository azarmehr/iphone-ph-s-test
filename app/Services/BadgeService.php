<?php

namespace App\Services;

use App\Events\BadgeUnlocked;
use App\Models\Badge;
use App\Models\User;

class BadgeService
{
    public function __construct(protected AchievementService $achievementService)
    {
    }

    public function checkForUserBadgeUpdate(User $user)
    {
        $totalAchievementsCount = $this->achievementService->calculateTotalUserAchievement($user);

        $achievableBadge = Badge::where('min_required_achievements', '<=', $totalAchievementsCount)
            ->orderBy('min_required_achievements', 'desc')
            ->first();

        $userHasAchievedBadge = $user->badges()
            ->wherePivot('badge_id', $achievableBadge->id)
            ->exists();

        if (! $userHasAchievedBadge){
            $user->badges()->attach($achievableBadge, [
                'created_at' => now(),
                'updated_at' => now()
            ]);

            BadgeUnlocked::dispatch($achievableBadge->title, $user);
        }
    }
}
