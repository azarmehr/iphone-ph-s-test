<?php

namespace App\Listeners;

use App\Services\BadgeService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CheckForUserBadgeUpdate
{
    /**
     * Create the event listener.
     */
    public function __construct(protected BadgeService $badgeService)
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(object $event): void
    {
        $this->badgeService->checkForUserBadgeUpdate($event->user);
    }
}
