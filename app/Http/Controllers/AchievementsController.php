<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\AchievementService;
use App\Services\UserService;
use Illuminate\Http\Request;

class AchievementsController extends Controller
{
    public function __construct(protected UserService $userService)
    {
    }

    public function index(User $user)
    {
        $currentBadge = $this->userService->getCurrentBadge($user);
        $nextBadge = $currentBadge->next_badge;

        return response()->json([
            'unlocked_achievements' => $this->userService->getUserAchievementsList($user),
            'next_available_achievements' => $this->userService->getUserNextAvailableAchievements($user),
            'current_badge' => $currentBadge->title,
            'next_badge' => $nextBadge->title,
            'remaing_to_unlock_next_badge' => $nextBadge->min_required_achievements - $currentBadge->min_required_achievements
        ]);
    }
}
