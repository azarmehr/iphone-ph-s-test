<?php

namespace Database\Seeders;

use App\Models\Badge;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BadgesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('badges')->truncate();

        $badges = [
            [
                'title' => 'Beginner',
                'slug' => 'beginner',
                'order' => 1,
                'min_required_achievements' => 0
            ],
            [
                'title' => 'Intermediate',
                'slug' => 'intermediate',
                'order' => 2,
                'min_required_achievements' => 4
            ],
            [
                'title' => 'Advanced',
                'slug' => 'advanced',
                'order' => 3,
                'min_required_achievements' => 8
            ],
            [
                'title' => 'Master',
                'slug' => 'master',
                'order' => 4,
                'min_required_achievements' => 10
            ],
        ];

        foreach ($badges as $badge){
            Badge::create($badge);
        }
    }
}
