<?php

namespace Database\Seeders;

use App\Models\CommentAchievement;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentAchievementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('comment_achievements')->truncate();

        $commentAchievements = [
            [
                'title' => 'First Comment Written',
                'slug' => 'first-comment-written',
                'order' => 1,
                'min_required_comments' => 1,
            ],
            [
                'title' => '3 Comments Written',
                'slug' => '3-comments-written',
                'order' => 2,
                'min_required_comments' => 3,
            ],
            [
                'title' => '5 Comments Written',
                'slug' => '5-comments-written',
                'order' => 3,
                'min_required_comments' => 5,
            ],
            [
                'title' => '10 Comments Written',
                'slug' => '10-comments-written',
                'order' => 4,
                'min_required_comments' => 10,
            ],
            [
                'title' => '20 Comments Written',
                'slug' => '20-comments-written',
                'order' => 5,
                'min_required_comments' => 20,
            ]
        ];

        foreach ($commentAchievements as $commentAchievement){
            CommentAchievement::create($commentAchievement);
        }
    }
}
