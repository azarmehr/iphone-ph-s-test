<?php

namespace Database\Seeders;

use App\Models\LessonAchievement;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LessonAchievementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('lesson_achievements')->truncate();

        $lessonAchievements = [
            [
                'title' => 'First Lesson Watched',
                'slug' => 'first-lesson-watched',
                'order' => 1,
                'min_required_lesson' => 1,
            ],
            [
                'title' => '5 Lessons Watched',
                'slug' => '5-lessons-watched',
                'order' => 2,
                'min_required_lesson' => 5,
            ],
            [
                'title' => '10 Lessons Watched',
                'slug' => '10-lessons-watched',
                'order' => 3,
                'min_required_lesson' => 10,
            ],[
                'title' => '25 Lessons Watched',
                'slug' => '25-lessons-watched',
                'order' => 4,
                'min_required_lesson' => 25,
            ],[
                'title' => '50 Lessons Watched',
                'slug' => '50-lessons-watched',
                'order' => 5,
                'min_required_lesson' => 50,
            ],
        ];

        foreach ($lessonAchievements as $lessonAchievement){
            LessonAchievement::create($lessonAchievement);
        }
    }
}
